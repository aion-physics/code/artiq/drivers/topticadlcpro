toptica_wrapper
==================

**Thin wrapper for the Toptica DLC API**

*Charles Baynham 2023*

Thin wrapper for the Toptica DLC API

Development
-----------

This is a poetry project - to use it, first install poetry using your preferred
method or following the instructions at https://python-poetry.org/.

To learn how to use poetry, read their docs. The short version is:

* **Install all required packages into a new virtual environment**: `poetry install`

* **Run unit tests**: `poetry run pytest`

* **Add a new package**: e.g. `poetry add matplotlib`

* **Update package versions**: `poetry update`
