Use this folder to store jupyter notebooks or python scripts which you use in
the course of testing your code. These won't form part of the package or be used
for unit tests, but might be helpful to others.
