"""toptica_wrapper - Thin wrapper for the Toptica DLCpro driver"""

from .driver import TopticaDLCPro

__author__ = "Charles Baynham <charles.baynham@gmail.com>"
__all__ = ["TopticaDLCPro"]
__version__ = "0.2"
